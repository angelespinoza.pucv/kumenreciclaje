// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyB49bZo6aYXxs8PCQ1fkND1uMGX-oPVEMw",
    authDomain: "email-contactform-b259e.firebaseapp.com",
    projectId: "email-contactform-b259e",
    storageBucket: "email-contactform-b259e.appspot.com",
    messagingSenderId: "245569445030",
    appId: "1:245569445030:web:69b6a2ab7b2ef745e16867"
  },
};
